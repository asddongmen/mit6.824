package mr

//
// RPC definitions.
//
// remember to capitalize all names.
//

import "os"
import "strconv"

//
// example to show how to declare the arguments
// and reply for an RPC.
//

type ExampleArgs struct {
	X int
}

type ExampleReply struct {
	Y int
}

// Add your RPC definitions here.

// 大写字母开头表示包外可用
// WorkerId
type WorkArgs struct {
	WorkerId string
}

// 任务信息
type TaskInfo struct {
	IsFinished bool // 任务完成标志
	TaskId int // 当前worker所执行的任务的Id,注意:task和work是不一样的，work是线程，任务是map或者reduce任务
	FileName string // 当前worker所执行的任务的文件名
	MapReduce string // map/reduce
	FileNumber int // 任务应当读取和输出多少文件
}


// 完成信息，任务完成之后worker发生一个rpc通知master
type CommitInfo struct {
	WorkerId string
	TaskId int
	MapReduce string
}

type CommitReply struct {
	IsOk bool
}
// Cook up a unique-ish UNIX-domain socket name
// in /var/tmp, for the master.
// Can't use the current directory since
// Athena AFS doesn't support UNIX-domain sockets.
func masterSock() string {
	s := "/var/tmp/824-mr-"
	s += strconv.Itoa(os.Getuid())
	return s
}
